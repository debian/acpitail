#define SCHED_BATT	0	/* batteries */
#define SCHED_ACSTATE	1	/* ac state */
#define SCHED_ZONE	2	/* zones */
#define SCHED_FAN	3	/* fans */

#define N_SCHED		4

/* Default Interval */
#define DI_ACSTATE	300
#define DI_BATT		15
#define DI_ZONE		5
#define DI_FAN		5

typedef struct
{
	int what;
	time_t last_run;
	int interval;
} sched_t;

void sleep_untill_next_event(sched_t *psched, int n_sched);
