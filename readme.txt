Show battery/fan/temperature info in a tail-like style.


installation:
------------
type:
make install
and that's it!
This program requires libacpi by Nico Golde: 
http://ngolde.de/libacpi.html


MultiTail colorscheme:
---------------------
colorscheme:acpitail:Show temperature/battery/etc info
cs_re:magenta::
cs_re:magenta:-
cs_re:blue|blue,,bold:... ... [0-9]* ..:..:.. 2...
cs_re_val_less:red,,bold:5:^.* remaining capacity: .* .([0-9]*) minutes
cs_re_val_less:yellow:10:^.*remaining capacity: .* .([0-9]*) minutes
cs_re:red:error.*
cs_re_val_bigger:yellow:55:temperature.*: ([0-9]*)
cs_re_val_bigger:red,,bold:65:temperature.*: ([0-9]*)


For everything more or less related to 'acpitail', please feel free
to contact me on: folkert@vanheusden.com
