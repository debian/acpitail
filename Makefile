VERSION=0.1

DEBUG=-g #-fprofile-arcs -ftest-coverage # -pg -g
CFLAGS+=-Wall -O2 -DVERSION=\"${VERSION}\" $(DEBUG)
LDFLAGS+=-lacpi $(DEBUG)

OBJS=error.o at.o sched.o emit.o

all: acpitail

acpitail: $(OBJS)
	$(CC) -Wall -W $(OBJS) $(LDFLAGS) -o acpitail

install: acpitail
	cp acpitail /usr/local/sbin

clean:
	rm -f $(OBJS) acpitail core *.da *.gcov *.bb*

package: clean
	mkdir acpitail-$(VERSION)
	cp *.c *.h Makefile readme.txt license.txt acpitail-$(VERSION)
	tar czf acpitail-$(VERSION).tgz acpitail-$(VERSION)
	rm -rf acpitail-$(VERSION)
