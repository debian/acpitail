#include <time.h>
#include <unistd.h>

#include "sched.h"

#define min(x, y) ((x) < (y)?(x) : (y))

void sleep_untill_next_event(sched_t *psched, int n_sched)
{
	time_t now = time(NULL);
	int loop;
	int sleep_n = now + 3600;

	for(loop=0; loop<n_sched; loop++)
	{
		time_t cur_next_schedule = psched[loop].last_run + psched[loop].interval;

		sleep_n = min(sleep_n, cur_next_schedule - now);
		if (sleep_n < 0)
			sleep_n = 0;
	}

	if (sleep_n > 0)
		sleep(sleep_n);
}
